import Vue from "vue";
import VueRouter from "vue-router";

import AppVue from "./App.vue";
import Message from "@/message.vue";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      component: Vue.component("App", AppVue),
      children: [
        {
          path: "/",
          component: Vue.component("Message", Message)
        },
        {
          path: "test"
        }
      ]
    },
  ]
});

export default router;
