export const Permission = {
    specialState: {
        redirectToHome: "specialState.redirectToHome",
    allowAll: "specialState.allowAll",
    userLoggedIn: "specialState.userLoggedIn",
    userLoggedOff: "specialState.userLoggedOff"
    },
    conversationsManager: {
        viewList: "conversationsManager.viewList"
    }
}