import { Permission } from "@/permissions";
export const RolesAllowedPermissions = {
    USER_ANONYMOUS: [],
    USER_LOGGED: [
        Permission.conversationsManager
    ]
}