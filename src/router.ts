import Vue from "vue";
import VueRouter from "vue-router";

import AppVue from "./App";
import DefaultLayoutVue from "@/views/layouts/DefaultLayoutVue";
import NoLayoutVue from "@/views/layouts/NoLayoutVue";
import NotFoundPageVue from "@/views/pages/no-layout/NotFoundPageVue";
import LoginPageVue from "@/views/pages/no-layout/LoginPageVue";
import ChatPageVue from "./views/pages/chat/ChatPageVue.vue";
import store from './store';

import { Permission } from "@/permissions";
import AclManager from "@/AclManager";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      component: Vue.component("App", AppVue),
      children: [
        {
          path: "chat",
          redirect: "root",
          component: Vue.component("DefaultLayout", DefaultLayoutVue),
          children: [
            {
              path: "chat",
              component: Vue.component("ChatPageVue", ChatPageVue),
              meta: {
                permission: Permission.conversationsManager.viewList
              }
            },
            { path: "*", redirect: "/not-found" },
          ]
        },
        {
          path: "/",
          component: Vue.component("NoLayout", NoLayoutVue),
          children: [
            {
              path: "/",
              name: "login",
              component: Vue.component("LoginPage", LoginPageVue),
              meta: {
                permission: Permission.specialState.userLoggedOff
              }
            },
            {
              path: "/login",
              name: "login",
              component: Vue.component("LoginPage", LoginPageVue),
              meta: {
                permission: Permission.specialState.userLoggedOff
              }
            },
            {
              path: "not-found",
              component: Vue.component("NotFound", NotFoundPageVue),
              meta: {
                permission: Permission.specialState.allowAll
              }
            },
            { path: "*", redirect: "/not-found" }
          ]
        }
      ]
    },
  ]
});

router.beforeEach((to, from, next) => {
  const { isAllowed, redirectionRoute } = AclManager.hasUserAccessToPermission(
    to.meta.permission
  );

  if (isAllowed) {
    next();
  } else {
    next(redirectionRoute);
  }
});

export default router;
