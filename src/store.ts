import Vue from "vue";
import Vuex from "vuex";
import JwtDecode from "jwt-decode";
import Global from "@/utils/Global";

const axios = Global.instanceAxios;
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    errorMessage: "",
    status: "",
    token: localStorage.getItem("token") || "",
    user: {},
    userRole: "anonymousUser",
  },
  mutations: {
    reset_error(state) {
      state.errorMessage = "";
    },
    notify_error(state, errorMessage) {
      state.errorMessage = errorMessage;
    },
    auth_request(state) {
      state.status = "loading";
    },
    auth_success(state, data) {
      state.status = "success";
      state.token = data.token;
      state.user = data.user;
      state.userRole = data.user.role || "anonymousUser";
    },
    auth_error(state) {
      state.status = "error";
      state.status = "";
      state.token = "";
      state.user = {};
      state.userRole = "anonymousUser";
    },
    logout(state) {
      state.status = "";
      state.token = "";
      state.user = {};
      state.userRole = "anonymousUser";
    },
  },
  actions: {
    welcomeBack: ({commit}, token) => new Promise((resolve, reject) => {
      commit("auth_request");
      console.log("couuuouc", token)
      if (token) {
        const tokenData: {
          user?: object;
        } = JwtDecode(token) || {};
        console.log(tokenData)
        if (tokenData.user) {
          axios.defaults.headers.common["x-access-token"] = token;
          axios.defaults.headers.common.Pragma = "no-cache";
          axios.defaults.headers.common["Cache-Control"] = "no-cache";
          axios.defaults.headers.common.Expires = "Sat, 01 Jan 2000 00:00:00 GMT";
          commit("auth_success", {token, user: tokenData.user});
          resolve();
        } else {
          commit("auth_error");
          sessionStorage.removeItem("token");
        }
      } else {
        commit("auth_error");
        sessionStorage.removeItem("token");
        reject();
      }
    }),
    login({commit}, user) {
      return new Promise((resolve, reject) => {
        commit("auth_request");
        axios
          .post("/authentification", user)
          .then((response) => {
            const token = response.data.token || false;
            console.log(response)
            console.log(response.data)
            console.log(token)

            if (response.data && token) {
              const tokenData: {
                user?: object;
              } = JwtDecode(token) || {};
              console.log(tokenData)
              if (tokenData.user) {
                axios.defaults.headers.common["x-access-token"] = token;
                axios.defaults.headers.common.Pragma = "no-cache";
                axios.defaults.headers.common["Cache-Control"] = "no-cache";
                axios.defaults.headers.common.Expires = "Sat, 01 Jan 2000 00:00:00 GMT";
                sessionStorage.setItem("token", token);
                commit("auth_success", {token, user: tokenData.user});
                resolve(response.data);
              } else {
                commit("auth_error");
                sessionStorage.removeItem("token");
                resolve(response.data);
              }
            } else {
              console.log("ICCCICIIIII")
              console.log(response.data)
              commit("auth_error");
              sessionStorage.removeItem("token");
              resolve(response.data);
            }
          })
          .catch((err) => {
            commit("auth_error");
            sessionStorage.removeItem("token");
            resolve(err.response.data);
          });
      });
    },
    logout({commit}) {
      return new Promise((resolve, reject) => {
        commit("logout");
        sessionStorage.removeItem("token");
        delete axios.defaults.headers.common["x-access-token"];
        resolve(true);
      });
    },
  },
  getters: {
    isLoggedIn: (state) => state.userRole !== "anonymousUser",
    authStatus: (state) => state.status,
    errorMessage: (state) => state.errorMessage,
    user: (state) => state.user,
    userRole: (state) => state.userRole,
  },
});



