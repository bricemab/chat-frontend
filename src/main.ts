import "vue-awesome/icons";
// import "./polyfills";
import Vue from "vue";
import "vue-awesome/icons";
import Icon from "vue-awesome/components/Icon.vue";
import AppVue from "./App.vue";
import store from "./store";
import router from "./router";
import Buefy from "buefy";
import "buefy/dist/buefy.css";
import "@/styles/global.scss";

Vue.component("v-icon", Icon);

Vue.use(Buefy);

// Vue.use(Buefy, {
//     defaultIconComponent: "vue-fontawesome",
//     defaultIconPack: "fas",
// });

const token = sessionStorage.getItem("token");
if (token) {
    store.dispatch("welcomeBack", token).then(() => {
        // console.log("WELCOME BACK");
        //router.push({name: "root"});
    });
}

Vue.component("v-icon", Icon);
Vue.config.productionTip = false;
new Vue({
  router,
  store,
  render: (h: any) => h(AppVue),
}).$mount("#app");
